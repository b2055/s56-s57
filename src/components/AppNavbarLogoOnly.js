import { Fragment } from 'react';
import { Container, Navbar } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';


export default function AppNavbarLogoOnly() {

    return (
        <Fragment>

            <Container className="absolute-logo">
                <Navbar expand="lg" className="navbar-logo-solo">
                    <Navbar.Brand className="text-white" as={NavLink} to="/" exact>EMINA<small>MISE</small></Navbar.Brand>
                </Navbar>
            </Container>
        </Fragment>
    )
}
