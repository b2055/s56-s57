import { Navbar, Nav, NavItem, NavDropdown, MenuItem, Container } from 'react-bootstrap'
import { Fragment, useState, useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';

export default function AppNavbar() {

    return (

        <Navbar expand="lg" className="w-100">
            <Container className="h-100 w-100">
                <Navbar.Brand className="text-white">EMINA<small>MISE</small></Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav">Menu</Navbar.Toggle>
                <Navbar.Collapse className="justify-content-end">
                    <Nav className="ml-auto">
                        <Nav.Link className="home-link">Home</Nav.Link>
                        <Nav.Link>Illustrations</Nav.Link>
                        <Nav.Link>Services</Nav.Link>
                        <Nav.Link>About</Nav.Link>
                        <NavDropdown className="nav-item text-white" title="Shop" id="basic-nav-dropdown">
                            <NavDropdown.Item>Shop</NavDropdown.Item>
							<NavDropdown.Divider />
                            <NavDropdown.Item>Featured Art</NavDropdown.Item>
							<NavDropdown.Divider />
                            <NavDropdown.Item>Cart</NavDropdown.Item>
                            <NavDropdown.Divider />
                            <NavDropdown.Item>Checkout</NavDropdown.Item>
                        </NavDropdown>
						<Nav.Link>Contact</Nav.Link>
						<Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}