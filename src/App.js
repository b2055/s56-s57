import { Carousel, Container } from 'react-bootstrap'
import { BrowserRouter as Router } from 'react-router-dom'
import { Route, Switch } from 'react-router-dom'
import { Fragment } from 'react'
import './App.css';
import Home from './pages/Home';
import Login from './pages/Login';
import LoginAccount from './pages/LoginAccount';
import Register from './pages/Register';
import BGSlider from './components/BGSlider';

function App() {

  return (
    <Fragment>
      <BGSlider />
      <Router>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/loginAccount" component={LoginAccount} />
            <Route exact path="/register" component={Register} />
            <Route component={Error} />
          </Switch>
      </Router>

    </Fragment>

  );
}

export default App;
