import { Fragment } from 'react';
import { Container, Row, Col, Nav, Navbar } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import { NavLink, Link } from 'react-router-dom';
import AppNavbarLogoOnly from '../components/AppNavbarLogoOnly'


export default function Login() {

    return (
        <Fragment>

            <AppNavbarLogoOnly />

            <Container className="login-container">
                <Row className="h-100 d-flex align-items-center justify-content-center p-0 m-0">
                    <Col className="login-box col-md-4 col-sm-12 h-75 d-flex p-0 m-0">
                        <Row className="w-100 p-0 m-0">
                            <Col className="col-12 d-flex justify-content-center align-items-center">

                                <Navbar.Brand className="text-white login-box-logo d-flex justify-content-center align-items-center" as={NavLink} to="/" exact>EMINA<small>MISE</small></Navbar.Brand>
                                
                            </Col>

                            <Col className="col-12 d-flex justify-content-center">
                                <Row className="w-100 h-100 p-0 m-0">
                                    <Col className="col-12">
                                        <Nav.Link className="btn btn-login border-bottom w-100 h-50 rounded-0" as={NavLink} to="/loginAccount" exact><h1>Login</h1></Nav.Link>
                                    </Col>
                                    <Col className="col-12">
                                        <Nav.Link className="btn btn-dark border-bottom w-100 h-50 rounded-0" as={NavLink} to="/register" exact><h1>Create an Account</h1></Nav.Link>
                                    </Col>
                                </Row>
                            </Col>
                            
                            <Col className="col-12 d-flex justify-content-center align-items-center border-top">
                                
                                <p className="text-white disclaimer-text">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lobortis lacus a tellus mollis, vitae fringilla turpis imperdiet. Donec at lobortis augue.
                                </p>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>
        </Fragment>
    )
}
