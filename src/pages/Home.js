import { Fragment } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import AppNavbar from '../components/AppNavbar'

export default function Home() {

	return (

		<Fragment>
			<AppNavbar />

			<Container className="home-container d-flex justify-content-center align-items-center">

			
			<Row className="w-100 p-0 m-0 welcome-box">
				<Col className="col-12 p-0 m-0 d-flex justify-content-center align-items-center">
					<h1 className="text-white">Welcome</h1>
				</Col>
				<Col className="col-12 p-0 m-0 d-flex justify-content-center align-items-center">
					<h1 className="mb-4 text-white text-center">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...</h1>
				</Col>
				<Col className="col-12 p-0 m-0 d-flex justify-content-center align-items-center">
					<p className="mb-4 mb-md-5 text-white text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque consectetur lacinia felis a tincidunt. Orci varius natoque penatibus et magnis dis parturient montes</p>
				</Col>
				<Col className="col-12 p-0 m-0 d-flex justify-content-center align-items-center">

						<a className="btn btn-primary p-3 px-xl-4 py-xl-3">Shop Now</a>
						<a className="btn btn-white btn-outline-white p-3 px-xl-4 py-xl-3">View Arts</a>
				</Col>
			</Row>
			</Container>


		</Fragment>
	)
}
