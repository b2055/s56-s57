import { Fragment } from 'react';
import { Form, Container, Row, Col, Nav, Navbar } from 'react-bootstrap';
import { NavLink, Link } from 'react-router-dom';
import AppNavbarLogoOnly from '../components/AppNavbarLogoOnly'

export default function Register() {

	return (
		<Fragment>
			<AppNavbarLogoOnly />

			<Container className="register-container">
				<Row className="h-100 d-flex align-items-center justify-content-center p-0 m-0">
					<Col className="register-box col-md-4 col-sm-12 h-75 d-flex p-0 m-0">
						<Row className="w-100 p-0 m-0">
							<Col className="col-12 d-flex justify-content-center align-items-center">

								<Navbar.Brand className="text-white register-box-logo d-flex justify-content-center align-items-center" as={NavLink} to="/" exact>EMINA<small>MISE</small></Navbar.Brand>

							</Col>

							<Col className="col-12 d-flex justify-content-center">
								<Row className="w-100 h-100 p-0 m-0">
									<h4 className="text-center">Create an Account</h4>
									
									<Col className="col-12">
										<Form className="mt-3">
											<Form.Group className="mb-3" controlId="userEmail">
												<Form.Control
													placeholder="Email Address"
												/>
												<Form.Text className="text-muted">
												</Form.Text>
											</Form.Group>

											<Form.Group className="mb-3" controlId="password1">
												<Form.Control
													placeholder="Password"
												/>
											</Form.Group>
										</Form>
									</Col>

									<Col className="col-12 d-flex justify-content-center">
										<Row className="w-100 h-100 p-0 m-0">
											<Col className="col-12">
												<Nav.Link className="btn btn-login border-bottom w-100 h-75 rounded-0"><h1>Next</h1></Nav.Link>
											</Col>
										</Row>


									</Col>
									<Link className="text-center text-white" as={NavLink} to="/login" exact>Go back</Link>
								</Row>


							</Col>
							<Col className="col-12 d-flex justify-content-center align-items-center border-top">
								<p className="text-white disclaimer-text">
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lobortis lacus a tellus mollis, vitae fringilla turpis imperdiet. Donec at lobortis augue.
								</p>
								
							</Col>
						</Row>
					</Col>
				</Row>
			</Container>
		</Fragment>


	)
}
